import numpy as np
from matplotlib import pyplot as plt
from scipy import stats


dist = 2

def func(dist):
    rvs = []
    while len(rvs) < 500:
        val = np.random.normal(3.14, np.exp(dist) * 0.5, 1)
        if val > 0 and val < 6.28:
            rvs.append(val[0])
    return rvs

print(np.exp(0.01))
print(np.exp(0.001))

plt.hist(func(0.01), bins = 30, histtype = 'step')
plt.hist(func(1), bins = 30, histtype = 'step')
plt.show()



