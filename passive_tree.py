import numpy as np
import scipy.linalg
from matplotlib import pyplot as plt
import time
def func(dt, stim, pinc):
    a = 1e-4*np.array([1, 1, 1]);
    ell = np.array([2.5,2.5,2.5])/100;
    dx = .0001;
    N = np.array(ell/dx).astype(int);
    A3 = 2*np.pi*a[2]*dx;
    As = 4*np.pi*1e-6;
    rho = A3/As;
    R2 = 0.3;
    gL = 1/15;
    Cm = 1;
    tau = Cm/gL;
    lam = a/(2*R2*gL)/dx**2;#   % lambda^2
    r = a/a[2];
    Hd = np.hstack([2*lam[0]*np.ones((1,N[0])),2*lam[2]*np.ones((1,N[1])),2*lam[2]*np.ones((1,N[2]+1))])[0];
    Hd[0] = lam[0];
    Hd[N[0]] = lam[2];
    Hd[N[0]+N[2]] =  lam@r.T;
    Hd[-1] = rho*lam[2];
    Hlen = len(Hd)

    Hu = np.hstack([-lam[0]*np.ones(N[0]-1), [0], -lam[1]*np.ones(N[1]), -lam[2]*np.ones(N[2])])
    Hl = np.hstack([-lam[0]*np.ones(N[0]-1), [0], -lam[1]*np.ones(N[1] - 1), -r[1]*lam[1], -lam[2]*np.ones(N[2])])
    Hl[-1] = rho*Hl[-1]

    H = np.eye(Hlen)
    np.fill_diagonal(H, Hd)
    for i in range(0,Hlen-1):
        H[i, i+1] = Hu[i]
        H[i+1, i] = Hl[i]

    H[N[0]+ N[1], N[0]-1] = -r[0]*lam[0]
    H[N[0]-1, N[0]+ N[1]] = -lam[0] 

    I = np.eye(Hlen)

    Bb = I + (I+H) * (dt/tau/2)
    L, U = scipy.linalg.lu(Bb, permute_l = True)

    x2 = np.arange(0,ell[2], dx)
    x0 = np.arange(ell[2],ell[2] + ell[0], dx)
    x1 = np.arange(ell[2],ell[2] + ell[1], dx)

    v = np.zeros(Hlen)
    rhs = np.zeros_like(v)

    t = 0 
    tcnt = 0
    x = np.linspace(0, ell[2] + np.max(ell[0:1]), Hlen)

    eloc = np.round(Hlen*1e-4*stim['loc']/np.sum(ell)).astype(int)
    Nt = np.ceil(stim['Tfin']/dt).astype(int)

    vhot = np.zeros((1,Nt))
    stim['amp'] = stim['amp'] * dt/2 / A3 / Cm

    f0 = stim['amp'] * (t > stim['t1']) * (t < stim['t2'])
    t = dt
    f1 = stim['amp'] * (t > stim['t1']) * (t < stim['t2'])

    r = np.zeros(Hlen)
    for i in range(len(eloc)):
        r[eloc[i]] = f0[i] + f1[i]

    print(Nt)
    for j in range(1,Nt):

        v = np.linalg.solve(U, np.linalg.solve(L, r.T))

        vhot[0,j] = v[-1]
        t += dt
        f0 = f1
        f1 = stim['amp'] * (t > stim['t1']) * (t < stim['t2'])

        r = 2*v - r
        for i in range(len(eloc)):
            r[eloc[i]] = f0[i] + f1[i]

    t = np.linspace(0,stim['Tfin'], Nt)
    return t, vhot[0,:]

dt = 0.1
stim = {'t1':np.array([1,10,20]), 't2':np.array([2,11,21]), 'amp':1e-4*np.array([1,1,1]), 'loc': np.array([740, 700, 650]), 'Tfin':50}
pinc =1


plt.plot(func(dt, stim, pinc)[1])

    
stim = {'t1':np.array([1,10,20]), 't2':np.array([2,11,21]), 'amp':1e-4*np.array([1,1,1]), 'loc': np.array([140, 100, 250]), 'Tfin':50}

plt.plot(func(dt, stim, pinc)[1])
plt.show()


