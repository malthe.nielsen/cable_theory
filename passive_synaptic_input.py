import numpy as np
from matplotlib import pyplot as plt

cab = {'rad':1e-4,'ell':1e-1, 'dx':1e-3,'dt':0.05}
stim = {'t1':np.array([0,3]),'tau':np.array([1,1])/2,'Gsyn':100*np.array([1,1]),'loc':np.array([0.06,0.04]),'Tfin':10}
#  stim = {'t1':np.array([0]),'tau':np.array([1]),'Gsyn':100*np.array([1]),'loc':np.array([0.06]),'Tfin':10}
pinc = 1

Cm = 1;#		% micro F / cm^2
g_L = 1/15;# %0.3;     		% mS / cm^2
R_2 = .3;# %0.034;		% k Ohm cm
dx = cab['dx'];
dt = cab['dt'];
Nx = int(cab['ell']/dx);#                % patch length
A = 2*np.pi*cab['rad']*dx;#            % patch surface area
x = np.arange(dx/2,cab['ell']-dx/2, dx);#


Nt = int(stim['Tfin'] / dt)
v0 = np.zeros(Nx)

S_mat = 2 * np.eye(Nx)
for i in range(Nx-1):
    S_mat[i,i+1] = -1
    S_mat[i+1,i] = -1
S_mat[0,0] = 1
S_mat[-1,-1] = 1
S_mat = S_mat / dx**2

tau = Cm/g_L;
lamb = np.sqrt(cab['rad']/(2*R_2*g_L))
A = 2*np.pi*cab['rad']*dx;

e1 = np.zeros(Nx);
eloc = np.round(Nx*stim['loc']/cab['ell']) - 1;
print(eloc)
e_loc = np.zeros(Nx)
for i in range(len(eloc)):
    e_loc[int(eloc[i])] = 1

b_loc = np.eye(Nx)
b_loc = np.fill_diagonal(b_loc, e_loc)

vsyn = 70;
Iapp = stim['Gsyn']/g_L;


B_mat = (np.eye(Nx) + lamb**2*S_mat)/tau
B_mat = 2/dt * np.eye(Nx) + B_mat

dBe = []
for i in range(len(eloc)):
    dBe.append(B_mat[int(eloc[i]), int(eloc[i])])
dBe = np.array(dBe)

ellss = 1e-4; ass = 1e-5; Ash = 1e-8;
Rss = ellss*R_2/np.pi/ass**2; Gsh = g_L*Ash;
g1 = 1/Rss/Gsh;
g2 = 1/Rss/A/Cm;

w = np.zeros((Nt,len(eloc)));
vhot = np.zeros((len(eloc),Nt));
t = 0

c0 = Iapp*(t/stim['tau'])*np.exp(1-t/stim['tau'])*(t>stim['t1'])
xi0 = g2 - g1*g2/(2*tau/dt + 1 + g1 + c0);
w[0,:] = 0;

t = dt
c1 = Iapp*(t/stim['tau'])*np.exp(1-t/stim['tau'])*(t>stim['t1'])
f = g2*( (4*tau/dt + c1 - c0)*w[0] + vsyn*(c0+c1) )/( 2*tau/dt + 1 + c1 + g1);
xi1 = g2 - g1*g2/(2*tau/dt + 1 + g1 + c1);

r = np.zeros(Nx);
for i in range(len(eloc)):
    r[int(eloc[i])] = f[i];
    B_mat[int(eloc[i]), int(eloc[i])] = dBe[i] + xi1[i];

v1 = np.linalg.solve(B_mat, r)
for i in range(len(eloc)):
    w[1,i] = ( (2*tau/dt - 1 - c0[i] - g1)*w[0,i] + vsyn*(c0[i]+c1[i]) + (v0[int(eloc[i])] + v1[int(eloc[i])]).T*g1 )/ ( 2*tau/dt + 1 + c1[i] + g1)

for j in range(2,Nt):
    t += dt
    c0 = c1; xi0 = xi1; v0 = v1;
    c1 = Iapp*((t-stim['t1'])/stim['tau']) * np.exp(1-(t-stim['t1'])/stim['tau'])*(t>stim['t1']); 
    f = g2*( (4*tau/dt + c1 - c0)*w[j-1,:] + vsyn*(c0+c1) )/( 2*tau/dt + 1 + c1 + g1);
    xi1 = g2 - g1*g2/(2*tau/dt + 1 + g1 + c1);

    r = (4/dt)*v0 - r

    for i in range(len(eloc)):
        r[int(eloc[i])] += f[i] + (xi0[i] - xi1[i])*v0[int(eloc[i])];
        B_mat[int(eloc[i])-1, int(eloc[i])-1] = dBe[i] + xi1[i];

    v1 = np.linalg.solve(B_mat,r);
    for i in range(len(eloc)):
        w[j,i] = ( (2*tau/dt - 1 - c0[i] - g1)*w[j-1,i] + vsyn*(c0[i]+c1[i]) + (v0[int(eloc[i])] + v1[int(eloc[i])]).T*g1 ) / ( 2*tau/dt + 1 + c1[i] + g1);
        vhot[i,j] = v1[int(eloc[i])];


plt.plot(vhot[0,:], color = 'red', linestyle = 'dashed')
plt.plot(vhot[1,:], color = 'black', linestyle = 'dashed')

plt.plot(w[:,0], color = 'red')
plt.plot(w[:,1], color = 'black')
plt.show()















