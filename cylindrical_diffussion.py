import numpy as np
from matplotlib import pyplot as plt
from skimage import draw
import time

D = .0001
h = 100
r = 70
phi = 100
r_init = 0.01
r_total = .05
h_total = .1



dt = 0.0001
N_total = 500
t_total = dt*N_total

dr = (r_total - r_init) / r
dh = h_total / h
r_vec = np.linspace(r_init, r_total, r)
dphi = 2*r_vec*np.pi / phi 


space = np.zeros((r,h,phi))
dphi = np.full((r,h,phi), dphi[:,np.newaxis, np.newaxis])

def roll(arr, axis):
    w_pad = -2*arr +np.roll(arr, 1, axis) + np.roll(arr, -1, axis)
    return w_pad[1:-1, 1:-1, 1:-1]

def nabla(arr, dr, dh, dphi, dt, C):
    pad_arr = np.pad(arr, 1, 'constant')
    pad_arr[0, :, :] = pad_arr[2, :, :]
    pad_arr[:, :, 0] = pad_arr[:, :,-2]
    pad_arr[:, :,-1] = pad_arr[:, :, 1]
    return arr + C*dt/dr**2 * roll(pad_arr, 0) + C*dt/dh**2 * roll(pad_arr, 1) + C*dt/dphi**2 * roll(pad_arr, 2)

t = 0

num_firings = 1000
fire_coord = np.zeros((3,num_firings))
for i in range(num_firings):
    fire_coord[1,i] = np.random.randint(0,h,1)[0]
    fire_coord[2,i] = np.random.randint(0,phi,1)[0]

time_fire = np.round(np.random.uniform(0,t_total,num_firings), 5) 

num_syn = 100
syn_car = np.zeros((5,num_syn))
for i in range(num_syn):
    syn_car[1,i] = np.random.randint(5,h-5,1)[0]
    syn_car[2,i] = np.random.randint(0,phi,1)[0]
    syn_car[3,i] = np.random.uniform(0,2*np.pi,1)[0]

def local_ion(arr, car, l, num_syn, angle_list):
    coord = car[:3,:].astype(int)
    for index in range(num_syn):
        ion = np.mean(arr[0:l, coord[1, index]-l:coord[1, index]+l, (coord[2, index]-l):(coord[2, index]+l)%arr.shape[2]])
        prob = ion * np.exp(-abs(np.sin(car[3, index]))/.1)
        if prob > np.random.uniform(0,1,1)[0] and car[4,index] <= 0:
            print('fiiiiiiiire')
            arr[0, coord[1,index], coord[2,index]] = 1 
            car[4,index] = 1
            angle_list.append(car[3,index]%np.pi)
    car[4,:] -= 0.01
    return arr, car


a_list = []
for i in range(N_total):
    start = time.time()
    space = nabla(space, dr, dh, dphi, dt, D)
    active_at_t = np.where(time_fire == t)
    for j in range(active_at_t[0].shape[0]):
        coord = fire_coord[:, active_at_t[0][j]].astype(int)
        space[coord[0], coord[1], coord[2]] = 1

    space, syn_car = local_ion(space, syn_car, 2, num_syn, a_list)


    t += dt
    t = np.round(t,5)
    end = time.time()
    #  print(end-start)

#  plt.hist(a_list, bins = 100)
#  plt.show()
print(len(a_list))


    




