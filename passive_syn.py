import numpy as np
import scipy.linalg
from matplotlib import pyplot as plt
import time

def func(stim, pinc):
    dt = 0.05
    a = 1e-4*np.array([1, 1, 1]);
    ell = np.array([2.5,2.5,2.5])/100;
    dx = .0001;
    N = np.array(ell/dx).astype(int);
    A3 = 2*np.pi*a[2]*dx;
    As = 4*np.pi*1e-6;
    rho = A3/As;
    R2 = 0.3;
    gL = 1/15;
    Cm = 1;
    tau = Cm/gL;
    lam = a/(2*R2*gL)/dx**2;#   % lambda^2
    r = a/a[2];

    Hd = np.hstack([2*lam[0]*np.ones((1,N[0])),2*lam[2]*np.ones((1,N[1])),2*lam[2]*np.ones((1,N[2]+1))])[0];
    Hd[0] = lam[0];
    Hd[N[0]] = lam[2];
    Hd[N[0]+N[2]] =  lam@r.T;
    Hd[-1] = rho*lam[2];
    Hlen = len(Hd)

    Hu = np.hstack([-lam[0]*np.ones(N[0]-1), [0], -lam[1]*np.ones(N[1]), -lam[2]*np.ones(N[2])])
    Hl = np.hstack([-lam[0]*np.ones(N[0]-1), [0], -lam[1]*np.ones(N[1] - 1), -r[1]*lam[1], -lam[2]*np.ones(N[2])])
    Hl[-1] = rho*Hl[-1]

    H = np.eye(Hlen)
    np.fill_diagonal(H, Hd)
    for i in range(0,Hlen-1):
        H[i, i+1] = Hu[i]
        H[i+1, i] = Hl[i]

    H[N[0]+ N[1], N[0]-1] = -r[0]*lam[0]
    H[N[0]-1, N[0]+ N[1]] = -lam[0] 

    I = np.eye(Hlen)

    B = I + (I+H) * (dt/tau/2)
    #  L, U = scipy.linalg.lu(Bb, permute_l = True)

    x2 = np.arange(0,ell[2], dx)
    x0 = np.arange(ell[2],ell[2] + ell[0], dx)
    x1 = np.arange(ell[2],ell[2] + ell[1], dx)

    eloc = np.round(Hlen*1e-4*stim['loc']/np.sum(ell)).astype(int)
    dBe = np.zeros(len(eloc))
    for i in range(len(eloc)):
        dBe[i] = B[eloc[i], eloc[i]]

    v = np.zeros(Hlen)
    rhs = np.zeros_like(v)

    t = 0 
    tcnt = 0
    x = np.linspace(0, ell[2] + np.max(ell[0:1]), Hlen)

    Nt = np.ceil(stim['Tfin']/dt).astype(int)

    vrec = np.zeros((len(eloc)+1,Nt))
    Iapp = stim['Gsyn']*dt/2/A3/Cm
    Iapp[np.where(eloc == Hlen)] *= A3/As
        #  Iapp = Iapp*A3/As

    
    c0 = Iapp*(t/stim['tau'])*np.exp(1-t/stim['tau'])*(t>stim['t1'])
    t = dt
    c1 = Iapp*(t/stim['tau'])*np.exp(1-t/stim['tau'])*(t>stim['t1'])

    r = np.zeros(Hlen)
    for i in range(len(eloc)):
        r[eloc[i]] = stim['vsyn'][i]*(c0[i] + c1[i])

    for j in range(1,Nt):

        for i in range(len(eloc)):
            B[eloc[i], eloc[i]] = dBe[i] + c1[i]

        v = np.linalg.solve(B, r)
        
        for i in range(len(eloc)):
            vrec[i,j] = v[eloc[i]]
        vrec[-1,j] = v[-1]


        t += dt
        c0 = c1
        c1 = Iapp*((t - stim['t1'])/stim['tau'])*np.exp(1-(t-stim['t1'])/stim['tau'])*(t>stim['t1'])
        print(c1)

        r = 2*v - r
        for i in range(len(eloc)):
            r[eloc[i]] = stim['vsyn'][i]*(c0[i] + c1[i])

    t = np.linspace(0,stim['Tfin'], Nt)
    return t, vrec

dt = 0.1
stim = {
        't1':np.array([5, 1]),
        'tau':np.array([1, 1]) * 1/2,
        'Gsyn':1e-6*np.array([1,1]),
        'vsyn':70*np.array([1,1]),
        'loc': np.array([100, 50]),
        'Tfin':10
        }
pinc =1

t, vrec = func(stim, pinc)


plt.plot(t, vrec[0,:])
plt.plot(t, vrec[1,:])
plt.plot(t, vrec[2,:])
#  plt.plot(t, vrec[3,:])
plt.show()
