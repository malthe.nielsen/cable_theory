import numpy as np
import scipy.linalg
from matplotlib import pyplot as plt
import time

def func(stim, pinc):
    dt = 0.05
    a = 1e-4*np.array([1, 1, 1, 1, 1, 1, 1]);
    ell = np.array([2.5,2.5,2.5, 2.5, 2.5, 2.5, 2.5])/100;
    dx = .0001;
    N = np.array(ell/dx).astype(int);
    #  N = np.array([3,3,3,3,3,3,3])
    A3 = 2*np.pi*a[2]*dx;
    As = 4*np.pi*1e-6;
    rho = A3/As;
    R2 = 0.3;
    gL = 1/15;
    Cm = 1;
    tau = Cm/gL;
    lam = a/(2*R2*gL)/dx**2;#   % lambda^2
    r1 = a[0]/a[2];
    r2 = a[1]/a[2];
    r3 = a[2]/a[4];
    r4 = a[3]/a[4];
    r5 = a[4]/a[6];
    r6 = a[5]/a[6];

    c1 = r1*lam[0]**2 + r2*lam[1]**2 + lam[2]**2
    c2 = r3*lam[2]**2 + r4*lam[3]**2 + lam[4]**2
    c3 = r5*lam[4]**2 + r6*lam[5]**2 + lam[6]**2
    


    #  Hd = np.hstack([2*lam[0]*np.ones((1,N[0])),2*lam[2]*np.ones((1,N[1])),2*lam[2]*np.ones((1,N[2]+1))])[0];
    Hd = []
    for i in range(len(N)):
        if i == len(N)-1:
            Hd.append(2*lam[i]*np.ones((1,N[0]+1)))
        else:
            Hd.append(2*lam[i]*np.ones((1,N[0])))
    Hd = np.hstack(Hd)[0]
    print(Hd.shape)
    Hd[0] = lam[0];
    Hd[N[0]] = lam[1];
    Hd[N[0]+N[1]] =  c1;
    Hd[N[0]+N[1]+N[2]] =  lam[3];
    Hd[N[0]+N[1]+N[2]+N[3]] =  c2;
    Hd[N[0]+N[1]+N[2]+N[3]+N[5]] =  lam[5];
    Hd[N[0]+N[1]+N[2]+N[3]+N[4]+N[5]] =  c3;
    Hd[-1] = rho*lam[-1];
    Hlen = len(Hd)

    Hu = np.hstack([-lam[0]*np.ones(N[0]-1), [0],
                    -lam[1]*np.ones(N[1]),
                    -lam[2]*np.ones(N[2]-1), [0],
                    -lam[3]*np.ones(N[3]),
                    -lam[4]*np.ones(N[4]-1), [0],
                    -lam[5]*np.ones(N[5]),
                    -lam[6]*np.ones(N[6])])

    Hl = np.hstack([-lam[0]*np.ones(N[0]-1), [0],
                    -lam[1]*np.ones(N[1]-1), [-r2*lam[1]+100],
                    -lam[2]*np.ones(N[2]-1), [0],
                    -lam[3]*np.ones(N[3]-1), [-r4*lam[3]+100],
                    -lam[4]*np.ones(N[4]-1), [0],
                    -lam[5]*np.ones(N[5]-1), [-r6*lam[5]+100],
                    -lam[6]*np.ones(N[6])])
    
    #  Hu = np.hstack([-lam[0]*np.ones(N[0]-1), [0], -lam[1]*np.ones(N[1]), -lam[2]*np.ones(N[2])])
    #  Hl = np.hstack([-lam[0]*np.ones(N[0]-1), [0], -lam[1]*np.ones(N[1] - 1), -r[1]*lam[1], -lam[2]*np.ones(N[2])])
    Hl[-1] = -rho*lam[-1] #why isnt this negative???

    H = np.eye(Hlen)
    np.fill_diagonal(H, Hd)
    for i in range(0,Hlen -1):
        H[i, i+1] = Hu[i]
        H[i+1, i] = Hl[i]

    H[N[0]+ N[1], N[0]-1] = -r1*lam[0]
    H[N[0]-1, N[0]+ N[1]] = -lam[0] 

    H[N[0]+ N[1] + N[2] + N[3], N[0] +N[1] + N[2]-1] = -r3*lam[2]
    H[N[0] + N[1] + N[2]-1, N[0]+ N[1] + N[2] + N[3]] = -lam[2] 

    H[N[0]+ N[1] + N[2] + N[3] + N[4] + N[5], N[0] +N[1] + N[2] + N[3] + N[4]-1] = -r3*lam[4]
    H[N[0] + N[1] + N[2] + N[3] + N[4]-1, N[0]+ N[1] + N[2] + N[3] + N[4] + N[5]] = -lam[4] 
    #  print(Hl[-1])


    I = np.eye(Hlen)

    B = I + (I+H) * (dt/tau/2)

    x7 = np.arange(0,ell[6], dx)
    x6 = np.arange(ell[6], ell[6]+ell[5], dx)
    x5 = np.arange(ell[6], ell[6]+ell[4], dx)
    x4 = np.arange(ell[6] + ell[4], ell[6]+ell[4]+ell[3], dx)
    x3 = np.arange(ell[6] + ell[4], ell[6]+ell[4]+ell[2], dx)
    x2 = np.arange(ell[6] + ell[4] + ell[2], ell[6]+ell[4]+ell[2] + ell[1], dx)
    x1 = np.arange(ell[6] + ell[4] + ell[2], ell[6]+ell[4]+ell[2] + ell[0], dx)

    eloc = np.round(Hlen*1e-4*stim['loc']/np.sum(ell)).astype(int)
    dBe = np.zeros(len(eloc))
    for i in range(len(eloc)):
        dBe[i] = B[eloc[i], eloc[i]]

    v = np.zeros(Hlen)
    rhs = np.zeros_like(v)

    t = 0 
    tcnt = 0
    x = np.linspace(0, ell[2] + np.max(ell[:-1]), Hlen)

    Nt = np.ceil(stim['Tfin']/dt).astype(int)

    vrec = np.zeros((len(eloc)+1,Nt))
    Iapp = stim['Gsyn']*dt/2/A3/Cm
    Iapp[np.where(eloc == Hlen)] *= A3/As
        #  Iapp = Iapp*A3/As

    
    c0 = Iapp*(t/stim['tau'])*np.exp(1-t/stim['tau'])*(t>stim['t1'])
    t = dt
    c1 = Iapp*(t/stim['tau'])*np.exp(1-t/stim['tau'])*(t>stim['t1'])

    r = np.zeros(Hlen)
    for i in range(len(eloc)):
        r[eloc[i]] = stim['vsyn'][i]*(c0[i] + c1[i])

    for j in range(1,Nt):

        for i in range(len(eloc)):
            B[eloc[i], eloc[i]] = dBe[i] + c1[i]

        v = np.linalg.solve(B, r)
        
        for i in range(len(eloc)):
            vrec[i,j] = v[eloc[i]]
        vrec[-1,j] = v[-1]


        t += dt
        c0 = c1
        c1 = Iapp*((t - stim['t1'])/stim['tau'])*np.exp(1-(t-stim['t1'])/stim['tau'])*(t>stim['t1'])

        r = 2*v - r
        for i in range(len(eloc)):
            r[eloc[i]] = stim['vsyn'][i]*(c0[i] + c1[i])

    t = np.linspace(0,stim['Tfin'], Nt)
    return t, vrec

dt = 0.1
stim = {
        't1':np.array([0,0]),
        'tau':np.array([1, 1]) * 1/2,
        'Gsyn':1e-4*np.array([1,1]),
        'vsyn':70*np.array([1,1]),
        'loc': np.array([800, 900]),
        'Tfin':5
        }

#  stim = {
#          't1':np.array([1]),
#          'tau':np.array([1]) * 1/2,
#          'Gsyn':1e-6*np.array([1]),
#          'vsyn':70*np.array([1]),
#          'loc': np.array([600]),
#          'Tfin':10
#          }
pinc = 1

t, vrec = func(stim, pinc)
v1 = vrec[-1, :]


loc = stim['loc']
fig, plot = plt.subplots(2,1)
for i in range(len(loc)):
    plot[0].plot(t, vrec[i,:], label = f'loc = {loc[i]}')
plot[0].legend()
plot[1].plot(t, vrec[-1,:], label = 'Soma')
plt.legend()
plt.show()

stim = {
        't1':np.array([0,0]),
        'tau':np.array([1, 1]) * 1/2,
        'Gsyn':1e-4*np.array([1,1]),
        'vsyn':70*np.array([1,1]),
        'loc': np.array([800, 300]),
        'Tfin':5
        }
t, vrec = func(stim, pinc)
v2 = vrec[-1,:]

plt.plot(t, v1)
plt.plot(t, v2)
plt.show()
